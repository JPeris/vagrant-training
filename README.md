# vagrant-training

A set of slides to introduce Vagrant using reveal.js

## Table of contents
- [What is and why Vagrant?](#What is and why Vagrant?)
- [Installation](#Installation)
- [Hello Vagrant](#Hello Vagrant)
- [Boxes](#Boxes)
- [Synced Folders](#Synced Folders)
- [Provisioning](#Provisioning)
- [Networking](#Networking)
- [Sharing a machine](#Sharing a machine)
- [Teardown](#Teardown)
- [Topics not covered](#Topics not covered)
- [What 's next?](#What 's next?)

#### More reading
- [Vagrant](https://www.vagrantup.com/): Official website.


## Instructions


## Installation

The **basic setup** is for authoring presentations only. The **full setup** gives you access to all reveal.js features and plugins such as speaker notes as well as the development tasks needed to make changes to the source.

### Viewing the presentation

1. Open index.html in a browser to view it


### Serving the presentation

It requires that the presentation runs from a local web server. The following instructions will set up such a server as well as all of the development tasks needed to make edits to the vagrant-training presentation.

1. Install [Node.js](http://nodejs.org/) (1.0.0 or later)

2. Install [Grunt](http://gruntjs.com/getting-started#installing-the-cli)

4. Clone the vagrant-repository repository
   ```sh
   $ git clone https://bitbucket.org/JPeris/vagrant-training.git
   ```

5. Navigate to the reveal.js folder
   ```sh
   $ cd vagrant-training
   ```

6. Install dependencies
   ```sh
   $ npm install
   ```

7. Serve the presentation and monitor source files for changes
   ```sh
   $ grunt serve
   ```

8. Open <http://localhost:8000> to view your presentation

   You can change the port by using `grunt serve --port 8001`.

This presentation uses reveal.js, so you can observe the following folder structure.

### Folder Structure
- **css/** Core styles without which the project does not function
- **js/** Like above but for JavaScript
- **plugin/** Components that have been developed as extensions to reveal.js
- **lib/** All other third party assets (JavaScript, CSS, fonts)


## License

MIT licensed

Copyright (C) 2016 Hakim El Hattab, http://hakim.se